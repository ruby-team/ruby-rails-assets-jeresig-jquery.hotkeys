# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-jeresig--jquery.hotkeys/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-jeresig--jquery.hotkeys"
  spec.version       = RailsAssetsJeresigJqueryHotkeys::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "jQuery Hotkeys lets you watch for keyboard events anywhere in your code supporting almost any key combination."
  spec.summary       = "jQuery Hotkeys lets you watch for keyboard events anywhere in your code supporting almost any key combination."
  spec.homepage      = "http://github.com/jeresig/jquery.hotkeys"
  spec.licenses      = ["MIT", "GPL-2.0"]

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_dependency "rails-assets-jquery", ">= 1.4.2"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
